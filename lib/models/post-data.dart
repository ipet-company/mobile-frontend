import 'package:flutter/material.dart';

class PostData {
  final String id;
  final String userId;
  final String description;
  final String fileId;
  final DateTime date;

  PostData({this.id, this.userId, this.description, this.fileId, this.date});

  factory PostData.fromJson(Map<String, dynamic> json) {
    return PostData(
        id: json['_id'] as String,
        userId: json['userId'] as String,
        description: json['description'] as String,
        fileId: json['fileId'] as String,
        date: DateTime.parse(json['date']).toLocal());
  }

  Map<String, dynamic> toJson() => {
        'userId': userId,
        'description': description,
        'fileId': fileId,
        'date': date.toString(),
      };
}

class PostEditing {
  bool editing;
  TextEditingController descriptionController;

  PostEditing(String initDescription) {
    this.editing = false;
    this.descriptionController =
        new TextEditingController(text: initDescription);
  }
}
