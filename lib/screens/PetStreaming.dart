import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_vlc_player/flutter_vlc_player.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:http/http.dart';

import 'package:ipetapp/services/authentication.dart';
import 'package:firebase_auth/firebase_auth.dart';

class PetStreaming extends StatefulWidget {
  PetStreaming({Key key, this.auth}) : super(key: key);

  static const String routeName = "/PetStreamingScreen";
  final String title = "iPet Streaming";
  final BaseAuth auth;
  @override
  _PetStreamingState createState() => _PetStreamingState();
}

class _PetStreamingState extends State<PetStreaming> {
  VlcPlayerController controller;
  @override
  void initState() {
    super.initState();

    controller = new VlcPlayerController(onInit: () {
      controller.play();
    });

    streamLink = getPrivateUserLink();
    getUserEmail();
  }

  String user = "";
  Future<String> streamLink;

  Future<String> getPrivateUserLink() async {
    try {
      String email = await getUserEmail();

      String url =
          'https://ipet-api-gateway.herokuapp.com/streaming/listing/' + email;

      Response response = await get(url).timeout(new Duration(seconds: 10));

      String streamLink;
      if (response.body == "") {
        streamLink = "";
      } else {
        streamLink = response.body + "/video";
        streamingMonitor(response.body);
      }

      return streamLink;
    } catch (err) {
      return "";
    }
  }

  Future<String> getUserEmail() async {
    try {
      FirebaseUser firebaseuser = await widget.auth.getCurrentUser();
      return firebaseuser.email;
    } catch (e) {
      return "";
    }
  }

  void streamingMonitor(String userLink) async {
    Timer.periodic(new Duration(seconds: 10), (timer) async {
      try {
        Response response =
            await get(userLink).timeout(new Duration(seconds: 5));

        if (response.statusCode != 200) {
          timer.cancel();
          reload();
        }
      } catch (err) {
        timer.cancel();
        reload();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("My Pet Live"),
        ),
        body: RefreshIndicator(
          onRefresh: () async {
            reload();
          },
          child: SingleChildScrollView(
            physics: AlwaysScrollableScrollPhysics(),
            child: Container(
              height: MediaQuery.of(context).size.height,
              child: FutureBuilder<String>(
                future:
                    streamLink, // a previously-obtained Future<String> or null
                builder:
                    (BuildContext context, AsyncSnapshot<String> snapshot) {
                  List<Widget> children;
                  if (snapshot.data != "" &&
                      snapshot.connectionState == ConnectionState.done) {
                    children = <Widget>[
                      VlcPlayer(
                        url: snapshot.data,
                        controller: controller,
                        placeholder: Center(child: CircularProgressIndicator()),
                        aspectRatio: 16 / 9,
                      ),
                      SizedBox(height: 10),
                      Stack(children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(top: 10, left: 20),
                          child: Align(
                            alignment: Alignment.center,
                            child: FloatingActionButton(
                              onPressed: () async {
                                if (await takescreenshot()) {
                                  Scaffold.of(context).showSnackBar(SnackBar(
                                      content: Text('Picture Saved!')));
                                }
                              },
                              child: Icon(Icons.add_a_photo),
                            ),
                          ),
                        )
                      ])
                    ];
                  } else if (snapshot.data == "") {
                    children = <Widget>[
                      Align(
                        alignment: Alignment.center,
                        child: Icon(
                          Icons.error_outline,
                          color: Colors.red,
                          size: 60,
                        ),
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: Padding(
                          padding: EdgeInsets.only(top: 100),
                          child:
                              Text('Livestream unavailable, try again later'),
                        ),
                      )
                    ];
                  } else {
                    children = <Widget>[
                      Align(
                        alignment: Alignment.center,
                        child: SizedBox(
                          child: CircularProgressIndicator(),
                          width: 60,
                          height: 60,
                        ),
                      )
                    ];
                  }

                  return Container(
                    padding: EdgeInsets.only(top: 48, bottom: 96),
                    alignment: Alignment.center,
                    child: Stack(
                      children: children,
                    ),
                  );
                },
              ),
            ),
          ),
        ));
  }

  Future<bool> takescreenshot() async {
    if (await Permission.storage.request().isGranted) {
      var img = await controller.takeSnapshot();
      final result = await ImageGallerySaver.saveImage(img);
      return true;
    } else {
      showAlertDialogPermission(context, "Permission dennied",
          "In order to save pictures in your phone, you need to allow our app in your permission settings.");
      return false;
    }
  }

  void reload() async {
    setState(() {
      streamLink = getPrivateUserLink();
    });
  }

  showAlertDialogPermission(BuildContext context, titulo, mensagem) {
    // configura o button
    Widget okButton = FlatButton(
      child: Text("OK"),
      onPressed: () => Navigator.pop(context),
    );
    // configura o  AlertDialog
    AlertDialog alerta = AlertDialog(
      title: Text(titulo),
      content: Text(mensagem),
      actions: [
        okButton,
      ],
    );
    // exibe o dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alerta;
      },
    );
  }
}
