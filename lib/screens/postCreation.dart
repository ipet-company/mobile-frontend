import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:ipetapp/models/post-data.dart';
import 'package:ipetapp/services/authentication.dart';
import 'package:path/path.dart';

class PostCreationScreen extends StatefulWidget {
  static const String routeName = "/postCreation";
  PostCreationScreen({Key key, this.auth}) : super(key: key);

  final BaseAuth auth;
  @override
  State<StatefulWidget> createState() => _PostCreationScreen();
}

class _PostCreationScreen extends State<PostCreationScreen> {
  File _image;
  String _uploadedFileURL;
  TextEditingController descriptionController = new TextEditingController();
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
  }

  Future<String> getUserEmail() async {
    try {
      FirebaseUser firebaseuser = await widget.auth.getCurrentUser();
      return firebaseuser.email;
    } catch (e) {
      return "";
    }
  }

  Future chooseFile() async {
    await ImagePicker.pickImage(source: ImageSource.gallery).then((image) {
      setState(() {
        _image = image;
      });
    });
  }

  Future<String> storeImage() async {
    var fileid =
        'images/' + DateTime.now().toIso8601String() + basename(_image.path);
    StorageReference storageReference =
        FirebaseStorage.instance.ref().child(fileid);
    StorageUploadTask uploadTask = storageReference.putFile(_image);
    await uploadTask.onComplete;
    await storageReference.getDownloadURL().then((fileURL) {
      setState(() {
        _uploadedFileURL = fileURL;
      });
    });
  }

  Future<bool> submitPost() async {
    String url = 'https://ipet-api-gateway.herokuapp.com/posts';

    try {
      String userId = await getUserEmail();

      print(userId);

      final response = await post(url,
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
          },
          body: jsonEncode(new PostData(
                  userId: userId,
                  description: descriptionController.text,
                  fileId: _uploadedFileURL,
                  date: DateTime.now().toUtc())
              .toJson()));

      return response.statusCode == 201;
    } catch (err) {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("New Publication"),
        ),
        body: Builder(
            builder: (context) => Align(
                alignment: Alignment.center,
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.center,
                        child: _image == null
                            ? RaisedButton(
                                child: Text('Select a picture'),
                                onPressed: chooseFile,
                                color: Colors.cyan,
                              )
                            : Image.file(_image),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 30, right: 10, left: 10),
                        child: TextFormField(
                            controller: descriptionController,
                            decoration: InputDecoration(
                                border: OutlineInputBorder(),
                                labelText:
                                    'Write a description for your post...')),
                      ),
                      Padding(
                          padding: const EdgeInsets.symmetric(vertical: 16.0),
                          child: Center(
                            child: RaisedButton(
                              onPressed: () async {
                                // Validate returns true if the form is valid, or false
                                // otherwise.
                                if (_image == null) {
                                  // If the form is valid, display a Snackbar.
                                  Scaffold.of(context).showSnackBar(SnackBar(
                                      content:
                                          Text('Please select some picture')));
                                } else {
                                  await storeImage();
                                  if (_uploadedFileURL == null) {
                                    Scaffold.of(context).showSnackBar(SnackBar(
                                        content: Text(
                                            'Sorry, something went wrong. Try again later.')));
                                  } else {
                                    final success = await submitPost();

                                    if (success) {
                                      Scaffold.of(context).showSnackBar(
                                          SnackBar(
                                              content: Text(
                                                  'Publication created!')));

                                      Future.delayed(const Duration(seconds: 2),
                                          () {
                                        Navigator.pop(context);
                                      });
                                    } else {
                                      Scaffold.of(context).showSnackBar(SnackBar(
                                          content: Text(
                                              'Sorry, something went wrong. Try again later.')));
                                    }
                                  }
                                }
                              },
                              child: Text('Submit'),
                            ),
                          )),
                    ],
                  ),
                ))));
  }
}
