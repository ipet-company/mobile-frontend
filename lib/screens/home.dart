import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:ipetapp/screens/postCreation.dart';
import 'package:ipetapp/screens/PetStreaming.dart';
import 'package:intl/intl.dart';
import 'package:ipetapp/models/post-data.dart';

import 'package:ipetapp/services/authentication.dart';
import 'package:firebase_auth/firebase_auth.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key, this.auth, this.userId, this.logoutCallback})
      : super(key: key);

  final BaseAuth auth;
  final VoidCallback logoutCallback;
  final String userId;
  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
    posts = fetchPosts();
    getUserEmail();
  }

  String user = "";

  getUserEmail() async {
    try {
      FirebaseUser firebaseuser = await widget.auth.getCurrentUser();
      setState(() {
        user = firebaseuser.email;
      });
    } catch (e) {
      setState(() {
        user = "";
      });
    }
  }

  signOut() async {
    try {
      await widget.auth.signOut();
      widget.logoutCallback();
    } catch (e) {
      print(e);
    }
  }

  Drawer getNavDrawer(BuildContext context) {
    var headerChild = UserAccountsDrawerHeader(
      accountEmail: Text(user),
      accountName: Text("Bem vindo, "),
      currentAccountPicture: Image.asset('assets/images/icon.png'),
    );
    var aboutChild = AboutListTile(
        child: Text("About"),
        applicationName: "iPet",
        applicationVersion: "v1.0.0",
        applicationIcon: Icon(Icons.adb),
        icon: Icon(Icons.info));

    ListTile getNavItem(var icon, String s, String routeName) {
      return ListTile(
        leading: Icon(icon),
        title: Text(s),
        onTap: () {
          setState(() {
            // pop closes the drawer
            Navigator.of(context).pop();
            // navigate to the route
            Navigator.of(context).pushNamed(routeName);
          });
        },
      );
    }

    var myNavChildren = [
      headerChild,
      ListTile(
        leading: Icon(Icons.video_call),
        title: Text('My Pet Live', textAlign: TextAlign.left),
        onTap: () {
          setState(() {
            // pop closes the drawer
            Navigator.of(context).pop();
            // navigate to the route
            Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => PetStreaming(
                auth: widget.auth,
              ),
            ));
          });
        },
      ),
      ListTile(
          leading: Icon(Icons.backspace),
          title: Text('LogOut', textAlign: TextAlign.left),
          onTap: signOut),
      aboutChild
    ];

    ListView listView = ListView(children: myNavChildren);

    return Drawer(
      child: listView,
    );
  }

  List<PostData> parsePosts(String responseBody) {
    final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();

    return parsed.map<PostData>((json) => PostData.fromJson(json)).toList();
  }

  Future<List<PostData>> posts;
  List<PostEditing> postEditControllers;
  Future<List<PostData>> fetchPosts() async {
    String url = 'https://ipet-api-gateway.herokuapp.com/posts';

    List<PostData> usersPosts;
    try {
      final response = await get(url).timeout(new Duration(seconds: 10));

      usersPosts = parsePosts(response.body);

      if (postEditControllers == null) {
        refreshControllers(usersPosts);
      } else {
        bool isEditing = false;
        postEditControllers.forEach((postEditController) {
          if (postEditController.editing) {
            isEditing = true;
          }
        });

        if (!isEditing) {
          refreshControllers(usersPosts);
        }
      }
    } catch (err) {
      return Future.error(err);
    }

    return usersPosts;
  }

  void refreshControllers(List<PostData> usersPosts) {
    postEditControllers = new List<PostEditing>();
    usersPosts.forEach((post) {
      postEditControllers.add(new PostEditing(post.description));
    });
  }

  Future<void> reload() async {
    setState(() {
      posts = fetchPosts();
    });
  }

  Future<bool> editPost(PostData postData, PostEditing postEditing) async {
    String url = 'https://ipet-api-gateway.herokuapp.com/posts/';
    final response = await put(url + postData.id,
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(new PostData(
                userId: postData.userId,
                description: postEditing.descriptionController.text,
                fileId: postData.fileId,
                date: postData.date.toUtc())
            .toJson()));

    return response.statusCode == 200;
  }

  Future<bool> deletePost(String postId) async {
    String url = 'https://ipet-api-gateway.herokuapp.com/posts/';

    final result = await delete(url + postId);
    return result.statusCode == 200;
  }

  String getFormatedDate(DateTime date) {
    final formater = new DateFormat('dd/MM/yyy, hh:mm');

    DateTime now = DateTime.now().toLocal();

    int millisecondsNow = now.millisecondsSinceEpoch;
    int millisecondsDate = date.millisecondsSinceEpoch;

    int diffInMilliseconds = millisecondsNow - millisecondsDate;

    Duration diference = new Duration(milliseconds: diffInMilliseconds);
    if (diference.inSeconds < 60) {
      return "just now";
    } else if (diference.inMinutes < 60) {
      return diference.inMinutes.toString() + " minutes ago";
    } else if (diference.inHours < 24) {
      return diference.inHours.toString() + " hours ago";
    } else if (diference.inDays < 2) {
      return "yesterday";
    } else {
      return formater.format(date);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0XFFF7F9FB),
        appBar: AppBar(
          title: Text("iPet"),
        ),
        body: RefreshIndicator(
          onRefresh: reload,
          child: Container(
            child: FutureBuilder<List<PostData>>(
              future: fetchPosts(),
              builder: (BuildContext context,
                  AsyncSnapshot<List<PostData>> snapshot) {
                List<Widget> children;
                children = <Widget>[];
                if (snapshot.hasData) {
                  List<PostData> posts = snapshot.data;
                  children = <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Expanded(
                          child: ListView.builder(
                            itemCount: posts.length,
                            itemBuilder: (context, index) {
                              return Padding(
                                padding: EdgeInsets.only(bottom: 20),
                                child: Padding(
                                  padding: EdgeInsets.only(left: 10, right: 10),
                                  child: Card(
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: EdgeInsets.only(
                                              bottom: 5,
                                              top: 5,
                                              left: 5,
                                              right: 5),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Row(
                                                children: <Widget>[
                                                  Padding(
                                                    padding: EdgeInsets.only(
                                                        right: 5),
                                                    child: Icon(
                                                        Icons.person_outline),
                                                  ),
                                                  Text(posts[index].userId),
                                                ],
                                              ),
                                              Visibility(
                                                  visible: user ==
                                                      posts[index].userId,
                                                  child: PopupMenuButton<int>(
                                                    itemBuilder: (context) => [
                                                      PopupMenuItem(
                                                        value: 1,
                                                        child: Text("Edit"),
                                                      ),
                                                      PopupMenuItem(
                                                        value: 2,
                                                        child: Text("Delete"),
                                                      ),
                                                    ],
                                                    onSelected: (value) async {
                                                      if (value == 1) {
                                                        setState(() {
                                                          postEditControllers[
                                                                  index]
                                                              .editing = true;
                                                        });
                                                      } else if (value == 2) {
                                                        if (await deletePost(
                                                            posts[index].id)) {
                                                          Scaffold.of(context)
                                                              .showSnackBar(SnackBar(
                                                                  content: Text(
                                                                      'Publication Deleted')));
                                                          reload();
                                                        } else {
                                                          Scaffold.of(context)
                                                              .showSnackBar(SnackBar(
                                                                  content: Text(
                                                                      'Sorry, something went wrong. Try again later.')));
                                                        }
                                                      }
                                                    },
                                                  ))
                                            ],
                                          ),
                                        ),
                                        Image.network(posts[index].fileId),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              bottom: 5,
                                              top: 5,
                                              left: 5,
                                              right: 5),
                                          child: Align(
                                            alignment: Alignment.centerLeft,
                                            child: Form(
                                              child: TextFormField(
                                                enabled:
                                                    postEditControllers[index]
                                                        .editing,
                                                maxLines: null,
                                                controller:
                                                    postEditControllers[index]
                                                        .descriptionController,
                                              ),
                                            ),
                                          ),
                                        ),
                                        Visibility(
                                          visible: postEditControllers[index]
                                              .editing,
                                          child: RaisedButton(
                                            onPressed: () async {
                                              if (await editPost(posts[index],
                                                  postEditControllers[index])) {
                                                Scaffold.of(context)
                                                    .showSnackBar(SnackBar(
                                                        content: Text(
                                                            'Publication Edited')));
                                              } else {
                                                Scaffold.of(context)
                                                    .showSnackBar(SnackBar(
                                                        content: Text(
                                                            'Sorry, something went wrong. Try again later.')));
                                              }

                                              setState(() {
                                                postEditControllers[index]
                                                    .editing = false;
                                              });
                                              reload();
                                            },
                                            child: Text("Done"),
                                          ),
                                        ),
                                        Padding(
                                          padding: EdgeInsets.only(
                                              bottom: 5,
                                              top: 5,
                                              left: 5,
                                              right: 5),
                                          child: Align(
                                            alignment: Alignment.centerRight,
                                            child: Text(getFormatedDate(
                                                posts[index].date)),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                        ),
                      ],
                    ),
                  ];
                } else if (snapshot.hasError) {
                  children = <Widget>[
                    Align(
                      alignment: Alignment.center,
                      child: Icon(
                        Icons.error_outline,
                        color: Colors.red,
                        size: 60,
                      ),
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: Padding(
                        padding: EdgeInsets.only(top: 100),
                        child: Text(
                            'iPet post feed unavailable, try again later.'),
                      ),
                    ),
                    ListView()
                  ];
                } else {
                  children = <Widget>[
                    Align(
                      alignment: Alignment.center,
                      child: SizedBox(
                        child: CircularProgressIndicator(),
                        width: 60,
                        height: 60,
                      ),
                    ),
                    ListView()
                  ];
                }

                return Container(
                  padding: EdgeInsets.only(top: 48, bottom: 96),
                  alignment: Alignment.center,
                  child: Stack(
                    children: children,
                  ),
                );
              },
            ),
          ),
        ),
        // Set the nav drawer
        drawer: getNavDrawer(context),
        floatingActionButton: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.bottomRight,
              child: FloatingActionButton(
                onPressed: () {
                  // pop closes the drawer
                  // Navigator.of(context).pop();
                  // navigate to the route
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => PostCreationScreen(
                      auth: widget.auth,
                    ),
                  ));
                  // Navigator.push(
                  //     context,
                  //     MaterialPageRoute(
                  //         builder: (context) => PostCreationScreen()));
                },
                child: Icon(Icons.add),
              ),
            ),
          ],
        ));
  }
}
