import 'package:flutter/material.dart';
import 'package:ipetapp/screens/postCreation.dart';
import 'package:ipetapp/screens/PetStreaming.dart';
import 'package:ipetapp/services/authentication.dart';
import 'package:ipetapp/screens/root_page.dart';

void main() {
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'iPets login',
      debugShowCheckedModeBanner: false,
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new RootPage(auth: new Auth()), // route for home is '/' implicitly
      routes: <String, WidgetBuilder>{
        // define the routes
        PetStreaming.routeName: (BuildContext context) => PetStreaming(),
        PostCreationScreen.routeName: (BuildContext context) =>
            PostCreationScreen(),
      },
    );
  }
}
